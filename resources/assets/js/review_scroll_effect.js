function initScroll(){
  var item = $('.reviewScrollEffect');
  var height = $(window).height();
  var menuItem = $('.review_menu');
  $(window).on('scroll', function(){
    var scrolltop = $(document).scrollTop();

    /* 스크롤 진행되면 해더에 그림자 효과 */
    if(scrolltop > 12){
      $(menuItem).addClass('active_shadow');
    }else{
      $(menuItem).removeClass('active_shadow');
    }

    for(var i =0; i<item.length; i++){
      var target = $(item[i]);
      var targetoffset = target.offset().top;
      if(targetoffset - height / 2 <= scrolltop){
        item.removeClass('active');
        target.addClass('active');
      }
    }
  });
}